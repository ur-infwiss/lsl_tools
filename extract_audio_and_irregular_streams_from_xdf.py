import argparse
import os.path
import csv
import numpy as np
import soundfile as sf

import xdf_helper


def normalize_columns(arr):
    arr /= max(np.abs(arr))


def fix_dtype(nparr):
    if channel_format in ['int8', 'int16', 'int32']:
        normalize_columns(nparr)  # scale to full intXXX range to minimize precision loss

    if channel_format == 'float32':
        nparr = nparr.astype(np.dtype(np.float32), copy=False)
    elif channel_format == 'double64':
        nparr = nparr.astype(np.dtype(np.float64), copy=False)
    elif channel_format == 'int8':
        nparr *= (pow(2, 7) - 1)
        nparr = nparr.astype(np.dtype(np.int8), copy=False)
    elif channel_format == 'int16':
        nparr *= (pow(2, 15) - 1)
        nparr = nparr.astype(np.dtype(np.int16), copy=False)
    elif channel_format == 'int32':
        nparr *= (pow(2, 31) - 1)
        nparr = nparr.astype(np.dtype(np.int32), copy=False)

    return nparr


parser = argparse.ArgumentParser(description='Extracts all audio streams and streams with nominal_srate == 0' +
                                             ' from an XDF file into separate WAV and CSV files.')
parser.add_argument(dest='xdf_path', nargs='+', type=str,
                    help='The path(s) to the xdf[z] file(s)')
parser.add_argument('--file-postfix', '-px', dest='postfix', type=str, default='',
                    help="A postfix added to all extracted files")
parser.add_argument('--delimiter', '-d', dest='delimiter', type=str, default=',',
                    help="The delimiter char to be used in the CSVs (default: ,)")
parser.add_argument('--quotechar', '-q', dest='quotechar', type=str, default='|',
                    help="The quote char to be used in the CSVs (default: |)")
parser.add_argument('-f', '--force', action='store_true', dest='overwrite_file',
                    help='Forces the output file(s) to override existing files.')
args = parser.parse_args()

for xdf_path in args.xdf_path:
    print('Loading XDF', xdf_path, "...")
    xdf_data = xdf_helper.XdfHelper(xdf_filename=xdf_path, pickle_xdf=False)
    print('Loading finished.')

    irregular_streams = xdf_data.get_all_streams_by_info(nominal_srate='0')
    for index, irregular_stream in enumerate(irregular_streams):
        name = irregular_stream['info']['name'][0]
        print('Processing stream', name, '...')

        channel_count = int(irregular_stream['info']['channel_count'][0])
        channel_format = irregular_stream['info']['channel_format'][0]
        out_filename = os.path.splitext(xdf_path)[0] + "_" + str(index) + "_" + name + args.postfix + ".csv"

        if not args.overwrite_file and os.path.exists(out_filename):
            print('The destfile \'' + out_filename + '\' already exists and must not be overwritten. '
                                                     'Choose another postfix or set the --force flag.')
            continue

        time_stamps = irregular_stream['time_stamps']
        time_series = irregular_stream['time_series']

        print('Writing stream', name, 'to file', out_filename, '...')
        with open(out_filename, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file,
                                    delimiter=args.delimiter,
                                    quotechar='|',
                                    quoting=csv.QUOTE_MINIMAL)
            for sample_index, sample in enumerate(time_series):
                time_stamp = time_stamps[sample_index]
                csv_writer.writerow([time_stamp] + sample)

    audio_streams = xdf_data.get_all_streams_by_info(type='Audio')
    for index, audio_stream in enumerate(audio_streams):
        name = audio_stream['info']['name'][0]
        print('Processing stream', name, '...')

        sample_rate = int(audio_stream['info']['nominal_srate'][0])
        channel_count = int(audio_stream['info']['channel_count'][0])
        channel_format = audio_stream['info']['channel_format'][0]
        out_filename = os.path.splitext(xdf_path)[0] + "_" + str(index) + "_" + name + args.postfix + ".wav"

        if not args.overwrite_file and os.path.exists(out_filename):
            print('The destfile \'' + out_filename + '\' already exists and must not be overwritten. '
                                                     'Choose another postfix or set the --force flag.')
            continue

        data = audio_stream['time_series']

        # xdf always returns a numpy float array - do some data conversion
        fixed_data = fix_dtype(data)

        print('Writing stream', name, 'to file', out_filename, '...')
        sf.write(out_filename, fixed_data, sample_rate)
