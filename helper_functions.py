import subprocess
import sys


def get_git_revision_short_hash():
    try:
        return subprocess.check_output(
            ['gitx', 'rev-parse', '--short', 'HEAD'],
            stderr=subprocess.STDOUT).decode(
            sys.stdout.encoding).strip()
    except subprocess.CalledProcessError as e:
        return "GIT_QUERY_FAILED"
    except FileNotFoundError as e:
        return "GIT_NOT_AVAILABLE"
