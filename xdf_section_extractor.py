from typing import Dict, Tuple, List, Any, Optional
import logging
import pandas as pd
import pprint
import xdf_errors
import xdf_helper


class XdfMarker:
    """
    This Class simplifies the access to markers in the lsl-stream red from a
    xdf-file.
    """
    def __init__(self,
                 xdf_handler: xdf_helper.XdfHelper,
                 marker_filter: Optional[List[str]] = None,
                 ending_marker: str = "S 17",
                 **kwargs: str) -> None:
        """
        Selects the marker stream from an already imported xdf-file

        :param xdf_handler: A xml-filme imported with XdfHelper Class.
        :param marker_filter:
                       An optional list of markers that will be used.
                       Defaults to None so all markers will be extracted.
        :param ending_marker:
                       The marker label that indicates the endpoint of each
                       marked section.
        :param kwargs: These kwargs are passed to the
                       XdfHelper.get_first_stream_by_info Method to select
                       the marker stream
        """
        self.__xdf_filename: str = xdf_handler.xdf_filename
        xdf_stream: Dict[str, Any]
        xdf_stream = xdf_handler.get_first_stream_by_info(**kwargs)
        self.__time_series = xdf_stream['time_series']
        self.__time_stamps = xdf_stream['time_stamps']
        logging.debug("Found marker stream with %s markers in file %s.",
                      len(self),
                      xdf_handler.xdf_filename)
        self.__marked_sectors: Dict[str, List[slice]]
        self.__marked_sectors = self.__collect_marked_sectors(ending_marker,
                                                              marker_filter)
        

    def __collect_marked_sectors(self,
                                 ending_marker: str,
                                 marker_filter: Optional[List[str]] = None
                                ) -> Dict[str, List[slice]]:
        if marker_filter and ending_marker in marker_filter:
            raise xdf_errors.XdfDataError("Parameter ending_marker should "
                                          "not appear in parameter "
                                          "marker_filter.")
        marked_sectors = {}
        sector_start = None
        sector_label = None
        for label, timestamp in zip(self.__time_series, self.__time_stamps):
            # Only the first element is used ([0]) because each time_series
            # element
            # is a list of labels for each channel (we only have one,
            # in practice.
            label = label[0]
            if label == ending_marker:
                if not sector_start:
                    logging.warning("End marker without start marker at "
                                    "position %s in file %s will be "
                                    "ignored.",
                                    timestamp,
                                    self.__xdf_filename)
                else:
                    if (not marker_filter) or (sector_label in marker_filter):
                        if sector_label not in marked_sectors:
                            marked_sectors[sector_label] = []
                        marked_sectors[sector_label].append(
                            slice(sector_start, timestamp))
                    sector_start = None
                    sector_label = None
            else:
                if sector_start and (not marker_filter
                                     or sector_label in marker_filter):
                    logging.warning("Section marker %s followed by marker "
                                    "%s instead of end marker at "
                                    "position %s in file %s will be "
                                    "ignored.",
                                    sector_label,
                                    label,
                                    timestamp,
                                    self.__xdf_filename)
                sector_start = timestamp
                sector_label = label
        if not marked_sectors:
            logging.error("No marked sections could be found in file %s.",
                          self.__xdf_filename)
        return marked_sectors

    def keys(self):
        """
        Get the collceted marker labels.

        :return: The items of this list can be used as key in
        the __getitem__-method.
        """
        return self.__marked_sectors.keys()

    def __getitem__(self, item: str) -> List[slice]:
        """
        Get slices of a specified marker label.

        :param item: The marker label that should be used for generating a
                     list of slices.
        :return: A list of slices that can each be be used to extract a section
                 from the feature data streams.
        """
        return self.__marked_sectors[item]

    def __len__(self) -> int:
        """
        Get the number of markers in the marker-stream.

        :return: number of markers
        """
        return len(self.__time_series)


class XdfSectionExtractor: # pylint: disable=too-few-public-methods
    """
    Helper class for extracting feature data from xdf files.

    This class assists in extracting slices that are classified by
    a marker stream.
    Data from a single xdf-file from multiple streams and
    multiple channels can be extracted.

    It is essential to know, that one xdf file consists of
    multiple streams (identified by a stream name) and
    each of these streams consists of one or more
    channels (identified by a channel label).
    """
    def __init__(self,
                 xdf_path: str,
                 marker_stream_info_selector: Dict[str, str],
                 feature_stream_info_selector: Dict[str, str],
                 feature_channel_labels: Optional[List[str]] = None,
                 marker_filter: Optional[List[str]] = None
                ) -> None:
        """
        Import a xdf file and extract feature and marker streams.

        Examples:
            >>> use_these_markers = ["S {:2}".format(i) for i in range(1, 17)]
            >>> xdf = XdfSectionExtractor("../koerperpositionen/vp7.xdf",
            >>>           {"type": "Markers"},
            >>>           {"type": "IMU"},
            >>>           ["X", "Y", "Z", "W"],
            >>>           marker_filter=use_these_markers)
            >>> # Get the first sample of the section marked by the label "S  1"
            >>> type(xdf["S  1"][0])
            <class 'dict'>

        :param xdf_path: The path to the xdf file that will be imported.
        :param marker_stream_info_selector: A Dictionary that will be used for
                 selecting the marker stream. A stream will be extracted that
                 contains all keys and given values of the dictionary in
                 its "info" field. If multiple channels match only the first
                 stream will be used as marker stream.
        :param feature_stream_info_selector: A Dictionary that will be used for
                 selecting the feature streams. All stream will be extracted
                 that contain all keys and given values of the dictionary in
                 their "info" field.
        :param feature_channel_labels: If this optional list is present
                 channels of the feature streams will only be used if their
                 labels appear in this list. Defaults to None that will import
                 all channels of the feature streams.
        :param marker_filter:
                       An optional list of markers that will be used.
                       Defaults to None so all markers will be extract
        """
        self.xdf_filename: str = xdf_path
        self.__marker_stream: XdfMarker
        feature_streams: List[Dict[str, Any]]
        self.__marker_stream, feature_streams = self.__import_streams(
            xdf_path,
            marker_stream_info_selector,
            feature_stream_info_selector,
            marker_filter)
        self.__stream_dataframes = (
            self.__create_dataframes(feature_streams,
                                     feature_channel_labels))

    def keys(self):
        """
        Get all marker labels that were extracted.

        This method is forwarded to the X

        :return:
        """
        return self.__marker_stream.keys()

    def __getitem__(
            self,
            marker_label: str
    ) -> List[Dict[str, pd.DataFrame]]:
        """
        Get all data from the feature channels in the feature streams that
        are classified by a specified marker label.

        :param marker_label: The marker label to extract
        :return:
            A list of items that are classified by markers.
            Each marker (of the given label) results in one item in this
            list. Each item contains a dictionary of the feature streams with
            stream names as keys. The values are pandas dataframes with
            each channel as a column.
            This structure results from the circumstance that each stream
            has its own time stamps resulting in its own framerate.
            Each stream contains one list of time stamps that is shared by
            its different channels.
            So a single datafrane cannot be used to store all streams.
            But all channles of one stream can be indexed by their time stamps
            in one pandas dataframe.
        """
        feature_data_by_marker = []
        marked_sectors = self.__marker_stream[marker_label]
        for marked_sector in marked_sectors:
            feature_sector = self.__cut_sector(marked_sector)
            feature_data_by_marker.append(feature_sector)
        return feature_data_by_marker

    def __cut_sector(self, sector_slice: slice) -> Dict[str, pd.DataFrame]:
        """
        Use a slice to extract the given time from all streams in all channels.

        :param key: Time slice of data that should be extracted from th streams.
        :return:
            The different streams are returned in a dictionary with
            stream names as keys. The values are pandas dataframes with
            each channel as a column.
            This structure results from the circumstance that each stream
            has its own time stamps resulting in its own framerate.
            Each stream contains one list of time stamps that is shared by
            its different channels.
            So a single datafrane cannot be used to store all streams.
            But all channles of one stream can be indexed by their time stamps
            in one pandas dataframe.
        """
        slices = {}
        for stream_name, stream_data in self.__stream_dataframes.items():
            slices[stream_name] = stream_data[sector_slice]
        return slices

    def __create_dataframes(self,
                            feature_streams: List[Dict[str, Any]],
                            filter_channel_labels: Optional[List[str]] = None):
        stream_dataframes = {}
        for stream in feature_streams:
            stream_name = XdfSectionExtractor.__get_stream_name(stream)
            time_stamps = stream["time_stamps"]
            time_series = stream["time_series"]
            if time_series is None or time_series.size == 0:
                logging.error("Stream %s in xdf file %s has no data",
                              stream_name,
                              self.xdf_filename)
            channel_labels = self.__get_channel_labels(stream)
            stream_dataframe = pd.DataFrame(time_series,
                                            columns=channel_labels,
                                            index=time_stamps)
            if filter_channel_labels:
                drop_columns = set(channel_labels) - set(filter_channel_labels)
                if drop_columns == set(channel_labels):
                    logging.error("Stream %s in xdf file %s has no channels "
                                  "that appear in filter channel labels %s.",
                                  stream_name,
                                  self.xdf_filename,
                                  filter_channel_labels)
                stream_dataframe.drop(columns=drop_columns, inplace=True)
            if stream_name in stream_dataframes:
                raise xdf_errors.XdfDataError("Stream {} exists more "
                                              "than once in xdf file {}."
                                              .format(stream_name,
                                                      self.xdf_filename))
            stream_dataframes[stream_name] = stream_dataframe
        return stream_dataframes

    def __import_streams(
            self,
            xdf_path: str,
            marker_stream_info_selector: Dict[str, str],
            feature_stream_info_selector: Dict[str, str],
            marker_filter: Optional[List[str]] = None
    ) -> Tuple[XdfMarker, List[Dict[str, Any]]]:
        logging.debug("Opening xdf file %s.", xdf_path)
        xdf_handler = xdf_helper.XdfHelper(xdf_filename=xdf_path,
                                           pickle_xdf=True,
                                           synchronize_clocks=True,
                                           handle_clock_resets=True,
                                           dejitter_timestamps=True)
        marker_stream = XdfMarker(xdf_handler,
                                  marker_filter,
                                  **marker_stream_info_selector)
        feature_streams = xdf_handler.get_all_streams_by_info(
            **feature_stream_info_selector)
        logging.debug("Found %s feature streams in file %s.",
                      len(feature_streams), self.xdf_filename)
        return marker_stream, feature_streams

    @staticmethod
    def __get_stream_name(
            feature_stream: Dict[str, Any]) -> str:
        """
        Extract stream name.

        :param feature_stream: One particular stream of the xdf file.
        :return: The name of the stream.
        """
        return "".join(feature_stream['info']["name"][0])

    @staticmethod
    def __get_channel_labels(
            feature_stream: Dict[str, Any]) -> List[str]:
        """
        Extracts a list of all channel labels of a stream.

        :param feature_stream: The lsl stream with multiple channels to extract.
        :return: A list of all channel labels (each converted to a string).
        """
        channel_infos = (
            feature_stream["info"]["desc"][0]["channels"][0]["channel"])
        return ["".join(info["label"]) for info in channel_infos]
