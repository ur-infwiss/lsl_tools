import pyxdf as xdf
import os
import pickle
import lz4.frame
import hashlib
from typing import Any, List, Callable, Tuple, Dict
from xdf_errors import *


class XdfHelper(object):
    def __init__(self,
                 xdf_filename: str,
                 pickle_xdf: bool = False,
                 *args,
                 **kwargs) -> None:
        """Loads an XDF file.

        Wraps the loaded XDF file using the xdf library and provides
        high-level helper
        functions.

        Caches the file loading with pickle.

        Args:
            xdf_filename: The path to the xdf-file
                (will be passed to pyxdf.load_xdf()).
            pickle_xdf: If the imported xdf-file should be pickled.
            *args: Will be passed on to xdf-load_xdf.
            **kwargs: Will be passed on to pyxdf.load_xdf().
        """
        self.xdf_filename: str = xdf_filename
        self.streams: List[Dict[str, Any]]
        self.fileheader: Dict[str, Any]
        if pickle_xdf:
            pickle_filename = "{}_{}.pklz".format(
                                        xdf_filename,
                                        XdfHelper.__dict_to_hash(kwargs))
            if os.path.isfile(pickle_filename):
                with lz4.frame.open(pickle_filename, "rb") as pickle_file:
                    self.streams, self.fileheader = pickle.load(pickle_file)
            else:
                self.streams, self.fileheader = self.__import_xdf_file(
                                                        filename=xdf_filename,
                                                        *args,
                                                        **kwargs)
                with lz4.frame.open(pickle_filename, "wb") as pickle_file:
                    pickle.dump((self.streams, self.fileheader), pickle_file)
        else:
            self.streams, self.fileheader = self.__import_xdf_file(
                                                        filename=xdf_filename,
                                                        *args,
                                                        **kwargs)

    @staticmethod
    def __dict_to_hash(dictionary: Dict[str, str]) -> str:
        key_value_string: str = ""
        for key in sorted(dictionary.keys()):
            key_value_string += str(key)
            key_value_string += str(dictionary[key])
        return hashlib.md5(key_value_string.encode("utf-8")).hexdigest()

    @staticmethod
    def __import_xdf_file(
                  *args,
                  **kwargs) -> Tuple[List[Dict[str, Any]], Dict[str, Any]]:
        # kwargs["verbose"] = False
        streams, fileheader = xdf.load_xdf(*args, **kwargs)
        return streams, fileheader

    def get_all_streams_by_predicate(
           self,
           predicate: Callable[[Dict[str, Any]], bool]) -> List[Dict[str, Any]]:
        return list(filter(predicate, self.streams))

    def get_first_stream_by_predicate(
           self,
           predicate: Callable[[Dict[str, Any]], bool]) -> Dict[str, Any]:
        all_streams = self.get_all_streams_by_predicate(predicate)
        if not all_streams:
            raise StreamPredicateNotFoundError(self.xdf_filename, predicate)
        return all_streams[0]

    def get_all_streams_by_info(self,
                                **kwargs: Dict[str, str]) -> List[Dict[str, Any]]:
        def check_stream(stream: Dict[str, Any]) -> bool:
            for kw in kwargs:
                if stream['info'][kw][0].lower() != kwargs[kw].lower():
                    return False
            return True

        return self.get_all_streams_by_predicate(check_stream)

    def get_first_stream_by_info(self,
                                 **kwargs: Dict[str, str]) -> Dict[str, Any]:
        all_streams = self.get_all_streams_by_info(**kwargs)
        if not all_streams:
            raise StreamInfoNotFoundError(self.xdf_filename, kwargs)
        return all_streams[0]
