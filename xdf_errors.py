import inspect
from typing import Any, Callable, Dict


class XdfError(Exception):
    pass


class StreamPredicateNotFoundError(XdfError):
    def __init__(self,
                 xdf_filename: str,
                 predicate: Callable[[Dict[str, Any]], bool]) -> None:
        self.xdf_filename: str = xdf_filename
        self.predicate: Callable[[Dict[str, Any]], bool] = predicate

    def __str__(self) -> str:
        error_msg = ("Could not find stream in file"
                     "{} that matches predicate {}.")
        return error_msg.format(self.xdf_filename,
                                inspect.getsource(self.predicate))


class StreamInfoNotFoundError(XdfError):
    def __init__(self, xdf_filename: str, stream_info: Dict[str, str]) -> None:
        self.xdf_filename: str = xdf_filename
        self.stream_info: Dict[str, str] = stream_info

    def __str__(self) -> str:
        return "Could not find stream in file {} with stream info {}.".format(
                    self.xdf.filename, self.stream_info)


class XdfDataError(XdfError):
    pass