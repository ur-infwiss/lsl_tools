import argparse
import pylsl
import functools
from typing import Dict, List, Tuple
import lsl_stream_processor as lsl_sp
from helper_functions import get_git_revision_short_hash

DEFAULT = 'default'


class SicherheitsverhaltenFusion(lsl_sp.StreamProcessor):
    MAX_VAL = 100.
    MIN_VAL = 0.

    DEFAULT_LSL_PREDICATE: str = (
        "(name ='openSMILE-features')"
    )

    CHANNEL_FILTER: Dict[str, List[str]] = {
        'openSMILE-features': ['pcm_RMSenergy_sma_amean'],
    }

    STREAM_CHANNEL_MIN_MAX: Dict[str, Dict[str, Tuple[float, float]]] = {
        'openSMILE-features': {
            DEFAULT: (0.01, 0.08)},
            #DEFAULT: (0.01, 0.15)},

    }

    STREAM_CHANNEL_WEIGHT: Dict[str, Dict[str, float]] = {
        'openSMILE-features': {
            DEFAULT: 1.},
    }

    def __init__(self, inlets_predicate: str, min_inlets: int) -> None:
        outlet_info: pylsl.StreamInfo = pylsl.StreamInfo(
            "Sensor fusion: sicherheitsverhalten", "fused sicherheitsverhalten", 1, 0, pylsl.cf_float32,
            source_id="git_short_hash=sicher_" + get_git_revision_short_hash())
        outlet_info.desc().append_child_value("manufacturer",
                                              "UR Informationswissenschaft")
        channels = outlet_info.desc().append_child("channels")
        angst_channel = channels.append_child("channel")
        angst_channel.append_child_value("label", "average sicherheitsverhalten")
        # Maybe we should provide these as parameters to init?
        self.channel_filter_ = SicherheitsverhaltenFusion.CHANNEL_FILTER
        self.stream_channel_min_max_ = SicherheitsverhaltenFusion.STREAM_CHANNEL_MIN_MAX
        self.stream_channel_weight_ = SicherheitsverhaltenFusion.STREAM_CHANNEL_WEIGHT
        super().__init__(outlet_info, inlets_predicate, min_inlets)

    @functools.lru_cache()
    def get_channel_list(
            self, inlet_info: pylsl.StreamInfo) -> Dict[str, int]:
        if inlet_info.name() not in self.channel_filter_:
            return {DEFAULT: 0}
        else:
            channel_labels = self.channel_filter_[inlet_info.name()]
            channel_lookup_table = {}
            for channel_label in channel_labels:
                if channel_label == "pcm_RMSenergy_sma_amean":
                    channel_number = 4177
                else:
                    channel_number = lsl_sp.get_channel_by_label(inlet_info,
                                                                 channel_label)
                channel_lookup_table[channel_label] = channel_number
            return channel_lookup_table

    def normalize(self, stream_name: str, channel_label: str,
                  value: float) -> float:
        min_value, max_value = None, None
        if stream_name in self.stream_channel_min_max_:
            channel_min_max = self.stream_channel_min_max_[stream_name]
            if channel_label in channel_min_max:
                min_value, max_value = channel_min_max[channel_label]
            elif DEFAULT in channel_min_max:
                min_value, max_value = channel_min_max[DEFAULT]
        if min_value is not None:
            if value < min_value:
                print("Stream {}, Channel {} sending {}. This is smaller than the minimum value ({}).".format(stream_name, channel_label, value, min_value))
                value = min_value
            if value > max_value:
                print("Stream {}, Channel {} sending {}. This exceeds the maximum value ({}).".format(stream_name, channel_label, value, max_value))
                value = max_value
            value -= min_value
            value /= (max_value - min_value)
        return value

    @functools.lru_cache()
    def get_weight_factor(self, stream_name: str, channel_label: str) -> float:
        weight = 1.
        if stream_name in self.stream_channel_weight_:
            channel_min_max = self.stream_channel_weight_[stream_name]
            if channel_label in channel_min_max:
                weight = channel_min_max[channel_label]
            elif DEFAULT in channel_min_max:
                weight = channel_min_max[DEFAULT]
        return weight

    def output_loop(self) -> None:
        newest_values: Dict[Tuple[str, str], float] = {}
        #print("1")
        while True:
            input_value: lsl_sp.StreamProcessor.INPUT_QUEUE_VALUE_TYPE
            input_value = self.input_queue.get()  # blocking read
            #print("2")
            inlet, sample, timestamp = input_value
            corrected_timestamp = timestamp # + inlet.time_correction()
            # the actual computation
            # print(inlet.info())
            channel_lookup: str = self.get_channel_list(inlet.info())
            stream_name: str = inlet.info().name()
            for channel_label, channel_number in channel_lookup.items():
                stream_channel_id = (stream_name, channel_label)
                new_value = sample[channel_number]
                print(stream_name,
                      channel_label,
                      new_value)
                new_value = self.normalize(stream_name,
                                           channel_label,
                                           new_value)
                newest_values[stream_channel_id] = new_value
                #print("Stream: {}, Value: {}".format(stream_name, new_value))
            value_sum = 0
            weight_sum = 0
            for (strea_name, channel_label), value in newest_values.items():
                weight = self.get_weight_factor(strea_name, channel_label)
                value_sum += weight * value
                weight_sum += weight
            average = value_sum / float(weight_sum)
            average = average * (SicherheitsverhaltenFusion.MAX_VAL - SicherheitsverhaltenFusion.MIN_VAL) + SicherheitsverhaltenFusion.MIN_VAL
            #  if average > SicherheitsverhaltenFusion.MAX_VAL:
            #      average = SicherheitsverhaltenFusion.MAX_VAL
            #  if average < SicherheitsverhaltenFusion.MIN_VAL:
            #      average = SicherheitsverhaltenFusion.MIN_VAL
            print(corrected_timestamp, average)
            self.outlet.push_sample([average], corrected_timestamp)


DEFAULT_MIN_NUMBER_OF_STREAMS = 1
parser = argparse.ArgumentParser(
    description='Computes the average of multiple streams.')
parser.add_argument("--min_number_of_streams", "-n",
                    dest="min_number_of_streams", type=int,
                    default=DEFAULT_MIN_NUMBER_OF_STREAMS,
                    help="Wait for at least this number of streams to be "
                         "available "
                         "before outputting their average, default: %d" %
                         DEFAULT_MIN_NUMBER_OF_STREAMS)
parser.add_argument("--predicate", "-p", dest='predicate', type=str,
                    default=SicherheitsverhaltenFusion.DEFAULT_LSL_PREDICATE,
                    help="The predicate that selects all relevant streams to "
                         "be averaged ."
                         "Default: {}".format(
                        SicherheitsverhaltenFusion.DEFAULT_LSL_PREDICATE))
args = parser.parse_args()

sicherheitsverhalten_fusion = SicherheitsverhaltenFusion(args.predicate, args.min_number_of_streams)
sicherheitsverhalten_fusion.output_loop()
