import glob
import os
import pickle
import lz4
import numpy as np
import pandas as pd
import logging
import xdf_errors
from typing import Dict, List, Optional
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeClassifier
import pprint
import xdf_section_extractor
from sklearn.tree import export_graphviz
import pydot
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import quaternion
#from sklearn.externals import joblib
import joblib


import classifier_config
import classifier_data_processing

logging.basicConfig(level=logging.INFO)


class SectionClassifier:
    POSES = {
        'S  1':  'koerper_links',
        'S  2':  'koerper_rechts',
        'S  3':  'oberkoerper_links',
        'S  4':  'oberkoerper_rechts',
        'S  5':  'kopf_links',
        'S  6':  'kopf_rechts',
        'S  7':  'nach_hinten',
        'S  8':  'nach_vorne_gerade',
        'S  9':  'nach_vorne_gebeugt',
        'S 10':  'kopf_nicken',
        'S 11':  'kopf_schuetteln',
        'S 12':  'kopf_nacken',
        'S 13':  'kopf_brust',
        'S 14':  'brust_raus',
        'S 15':  'brust_rein',
        'S 16':  'verbally instructed freezing',
    }

    
    def __init__(self,
                 path: str,
                 marker_stream_info_selector: Dict[str, str],
                 feature_stream_info_selector: Dict[str, str],
                 feature_channel_labels: Optional[List[str]] = None,
                 marker_filter: Optional[List[str]] = None) -> None:
        # feature data contains:
        #
        # Dict[<label names>,
        #      List (of samples with that label) [
        #          Dict[
        #              <stream name>,
        #              <pandas dataframe with channels as rows>
        #              ]
        #      ]
        # ]
        self.feature_data: Dict[str, List[Dict[str, pd.DataFrame]]]
        pickle_filename = os.path.join(path, "all_sections.pklz")
        if os.path.isfile(pickle_filename):
            with lz4.frame.open(pickle_filename, "rb") as pickle_file:
                (self.feature_data, self.xdf_file_list) = pickle.load(pickle_file)
        else:
            file_pattern = os.path.join(path, "*.xdf")
            self.xdf_file_list = glob.glob(file_pattern)
            self.feature_data = self.__read_xdf_files(
                self.xdf_file_list,
                marker_stream_info_selector,
                feature_stream_info_selector,
                feature_channel_labels,
                marker_filter)
            with lz4.frame.open(pickle_filename, "wb") as pickle_file:
                pickle.dump((self.feature_data, self.xdf_file_list), pickle_file)
        x: np.ndarray
        y: np.ndarray
        x, y, feature_labels = self.__build_feature_vector()
        self.__classify(x,y, feature_labels)

    def __classify(self, x: np.ndarray, y: np.ndarray, feature_labels: List[str]):
        random_state = 1
        classifier_list = {
            #"KNeighborsClassifier": KNeighborsClassifier(n_neighbors = 3),
            "RandomForestClassifier": RandomForestClassifier(criterion='entropy', max_depth=6, n_estimators=100, max_features='auto', random_state = random_state),
            #"SVC": SVC(gamma = 'auto'),
            #"AdaBoostClassifier": AdaBoostClassifier(),
            #"MLPClassifier": MLPClassifier(max_iter = 1000), 
            #"GaussianProcessClassifier": GaussianProcessClassifier(1.0 * RBF(1.0)),
            #"QuadraticDiscriminantAnalysis": QuadraticDiscriminantAnalysis(),
            #"DecisionTreeClassifier": DecisionTreeClassifier(criterion='entropy', max_depth=None, random_state = random_state),
            }
        x = StandardScaler().fit_transform(x)
        print (y)
        
        """
        # PLOT
        pca = PCA(n_components=2, whiten=True, random_state=random_state)
        pca.fit(x)
        x = pca.transform(x)
        print(x.shape)
        print(y.shape)

        plt.figure(figsize=(12,8))
        ax = plt.subplot(111)
        colors = cm.rainbow(np.linspace(0, 1, len(set(y))))
        icons = ["o", "s","P","*","X","D"]
        icons = (icons*(int(len(y) / len(icons))+1))[:len(y)]
        for marker,c,m in zip(sorted(set(y)),colors,icons):
            plot_x = x[y==marker]
            ax.scatter(plot_x[:,0], plot_x[:,1], label=self.POSES[marker], color=c, marker=m)

        chartBox = ax.get_position()
        ax.set_position([chartBox.x0, chartBox.y0, chartBox.width*0.6, chartBox.height])
        ax.legend(loc='upper center', bbox_to_anchor=(1.35, 0.8), shadow=True, ncol=1)

        plt.figure(figsize=(8,8))
        for i, person in enumerate(sorted(self.xdf_file_list)):
            x_person = []
            for marker in set(y):
                x_person.append(x[y==marker][i])
            x_person=np.array(x_person)
            plt.scatter(x_person[:,0], x_person[:,1], label=person)
        plt.legend()
        plt.show()
        # PLOT
        """

        
        x_train, x_test, y_train, y_test = train_test_split(x ,y,
                                                            test_size=2/8,
                                                            random_state = random_state,
                                                            stratify=y)

        
        print ("Training-Size: {}, Test-Size: {}".format(len(y_train),len(y_test)))
        for classifier_name, classifier in classifier_list.items():
            classifier.fit(x_train, y_train)
            score_test = classifier.score(x_test, y_test)
            score_train = classifier.score(x_train, y_train)
            #dot_data = export_graphviz(classifier, out_file=None, feature_names=feature_labels)
            #graph = pydot.graph_from_dot_data(dot_data)[0] 
            #print(graph)
            #graph.write_png("tree.png")
            
            print("{}: Train: {}, Test: {}".format(classifier_name, score_train, score_test))
            y_pred = classifier.predict(x_test)
            #print(np.array(sorted(list(zip(y_test.tolist(), y_pred.tolist())), key = lambda x: x[0])))
            
            labels = np.unique(y)
            joblib.dump(classifier, classifier_name+'.pkl')
            # Laden mit:
            # classifier = joblib.load(filename)
            print(labels)
            print(confusion_matrix(y_test, y_pred, labels=labels))
            #print(feature_labels)
            with open(classifier_config.FEATURE_LABELS_FILENAME, "w") as feature_labels_file:
                feature_labels_file.write("\n".join(feature_labels))

    def __get_stream_channel_union_intersection(self):
        streams_intersection = None
        streams_union = set()
        channels_intersection = None
        channels_union = set()
        for label in self.feature_data.keys():
            for sample in self.feature_data[label]:
                streams = set(sample.keys())
                if streams_intersection is None:
                    streams_intersection = streams
                else:
                    streams_intersection &= streams
                streams_union |= streams
        for label in self.feature_data.keys():
            for sample in self.feature_data[label]:
                for stream in streams_intersection:
                    channels = set(sample[stream].columns)
                    if channels_intersection is None:
                        channels_intersection = channels
                    else:
                        channels_intersection &= channels
                    channels_union |= channels

        if not streams_intersection:
            raise xdf_errors.XdfDataError("No stream was found that exists "
                                          "in all samples so it could be "
                                          "used for creating the feature "
                                          "vectors.")
        if not channels_intersection:
            raise xdf_errors.XdfDataError("No channel was found that exists "
                                          "in all samples so it could be "
                                          "used for creating the feature "
                                          "vectors.")
        return (sorted(streams_union),
                sorted(streams_intersection),
                sorted(channels_union),
                sorted(channels_intersection))

    def __add_quaternion_channels(self, streams_intersection):
        for label in sorted(self.feature_data.keys()):
            for sample in self.feature_data[label]:
                for stream in streams_intersection:
                    table = sample[stream]
                    column_names = classifier_data_processing.add_quaternion_columns(table)
        return column_names

    def __build_feature_vector(self):
        (streams_union,
         streams_intersection,
         channels_union,
         channels_intersection
        ) = self.__get_stream_channel_union_intersection()
        if (streams_union != streams_intersection):
            logging.warning("Set of streams differs in samples."
                            "Some streams will be dropped for"
                            "creating feature vectors: %s",
                            set(streams_union)-set(streams_intersection))
        if (channels_union != channels_intersection):
            logging.warning("Set of channels differs in samples."
                            "Some channels will be dropped for"
                            "creating feature vectors: %s",
                            set(channels_union)-set(channels_intersection))
        if set(["X","Y","Z","W"]).issubset(channels_intersection):
            channels_intersection += self.__add_quaternion_channels(
                                        streams_intersection)
            channels_intersection = [c for c in channels_intersection if c not in ["X", "Y", "Z", "W"]]
        logging.info("Using channels %s", channels_intersection)
        y = []
        x = []
        feature_labels = []
        labels_created = False
        for label in sorted(self.feature_data.keys()):
            #print("                                                                          "+label)
            i=0
            for sample in self.feature_data[label]:
                #print(i)
                #print(self.xdf_file_list[i])
                i+=1
                feature_vector = []
                for stream in streams_intersection:
                    for channel in channels_intersection:
                        #print ("Channel: {}".format(channel))
                        #print ("Stream: {}".format(stream))
                        #print (sample[stream])
                        
                        feature_data_raw = sample[stream].loc[:,channel]
                        features, labels = classifier_data_processing._calc_features(np.array(feature_data_raw))
                        feature_vector += features
                        if not labels_created:
                            for l in labels:
                                feature_labels.append("{}~{}~{}".format(stream,
                                                                          channel,
                                                                          l))
                labels_created = True
                y.append(label)
                x.append(feature_vector)
        return np.array(x), np.array(y), feature_labels


    def __read_xdf_files(self,
                         xdf_file_list: List[str],
                         marker_stream_info_selector: Dict[str, str],
                         feature_stream_info_selector: Dict[str, str],
                         feature_channel_labels: Optional[List[str]] = None,
                         marker_filter: Optional[List[str]] = None):
        feature_data = {}
        for xdf_filename in xdf_file_list:
            xdf = xdf_section_extractor.XdfSectionExtractor(
                xdf_filename,
                marker_stream_info_selector,
                feature_stream_info_selector,
                feature_channel_labels,
                marker_filter = marker_filter)
            for marker in xdf.keys():
                if marker not in feature_data:
                    feature_data[marker] = []
                section_list = xdf[marker]
                for section in section_list:
                    streams_without_data = []
                    for stream in section:
                        if section[stream].size == 0:
                            streams_without_data.append(stream)
                    for del_this_stream in streams_without_data:
                        logging.warning("Deleting stream %s in "
                                        "marker section %s in file"
                                        "%s because it is empty.",
                                        del_this_stream,
                                        marker,
                                        xdf_filename)
                        del section[del_this_stream]
                feature_data[marker] += section_list
        return feature_data


use_these_markers = ["S {:2}".format(i) for i in range(1, 17)]
#use_these_markers = ["S 16", "S 14", "S 15", "S  8", "S  9", "S  7"]
# 1-6: Verschiedene Gruppen, 1+2 3+4 5+6 ; 1+2+3+4 5+6; links rechts

# 8+9 zusammen 
#use_these_markers = ["S {:2}".format(i) for i in range(14, 16)]
#use_these_markers = ["S  2", "S  6"]

channel_labels = classifier_config.CHANNEL_LABELS
stream_predicate = classifier_config.STREAM_PREDICATE

SectionClassifier(
            "../neue_imus/",
            {"type": "Markers"},
            stream_predicate,
            channel_labels,
            marker_filter = use_these_markers)
