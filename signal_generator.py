import argparse
import pylsl
import time


parser = argparse.ArgumentParser(description='Generates an artificial LSL stream with one or more channels.')
parser.add_argument("--sample_rate", "-s", dest="sample_rate", type=int,
                    default=10,
                    help="The generated sample rate, default: %d" % 10)
parser.add_argument("--name", "-n", dest="name", type=str,
                    required=True,
                    help="'name' of the stream")
parser.add_argument("--type", "-t", dest="type", type=str,
                    required=True,
                    help="'type' of the stream")
parser.add_argument(dest='channel_expressions', nargs='+', type=str,
                    default="sin",
                    help='The expression that generates the signal for a channel. '
                         'Can be specified multiple times to generate multiple channels. '
                         'Examples: "sin", "lambda t: sin(t/100)"')
args = parser.parse_args()

sample_rate = args.sample_rate
sampling_interval = 1.0 / sample_rate
channel_count = len(args.channel_expressions)

print(vars(args))

outlet_info = pylsl.StreamInfo(args.name, args.type,
                               channel_count, sample_rate, pylsl.cf_float32,
                               'args: ' + str(vars(args)))

outlet_info.desc().append_child_value("manufacturer", "UR Informationswissenschaft")
channels = outlet_info.desc().append_child("channels")
for expression in args.channel_expressions:
    channel_desc = channels.append_child("channel")
    channel_desc.append_child_value("expression", expression)

outlet = pylsl.StreamOutlet(outlet_info)

channel_functions = [eval(expression) for expression in args.channel_expressions]

last_sent_timestamp = pylsl.local_clock()
while True:
    next_timestamp = last_sent_timestamp + sampling_interval

    sleep_delta = next_timestamp - pylsl.local_clock()
    time.sleep(sleep_delta)

    last_sent_timestamp = next_timestamp
    samples = [function(last_sent_timestamp) for function in channel_functions]

    outlet.push_sample(samples, timestamp=last_sent_timestamp)
    # print(last_sent_timestamp, samples)
