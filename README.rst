*********************************
Fusionierung und Targetkonstrukte
*********************************

Im Folgenden soll eine Übersicht über die Script zur Fusionierung verschiedenen LSL-Streams sowie der Targetkonstrukte, die die fusionierten Streams nutzen, um dichotome Einschätzungen abzuleiten.

.. role:: python(code)
   :language: python

Allgemeine Erläuterungen zu Fusionsscripten
===========================================

Alle Scripte zur Fusionierung, lesen verschiedene Input Streams/Channels und fusionieren diese durch lineare Gewichtung (eine Ausnahme bildet aufmerksamkeit_fusion.py). Der Aufbau aller Scripte ist deshalb identisch. Um die Streams, Channel und Gewichte anzupassen, müssen einige Konstanten zu Beginn der Scripte angepasst werden:

Die Konstanten :python:`MIN_VAL` und :python:`MAX_VAL` geben den Wertebereich des Outputs an. Die aktuelle Vorgabe ist hier der Bereich von 0 bis 100.

:python:`DEFAULT_LSL_PREDICATE` spezifiziert das LSL-Predikat, das an das (frei verfügbare) Python-Package *pylsl* weitergereicht wird, um alle Input-Streams zu selektieren.
Ein einzelner Stream kann beispielsweise mit :python:`(name ='openSMILE-features')` ausgewählt werden, es ist aber weiterhin möglich, komplexere Auswahlkriterien anzugeben (siehe Doku zu *pylsl*).

Falls Streams existieren, die mehrere Channel enthalten, kann durch Angabe der Konstante :python:`CHANNEL_FILTER` eine Liste angegeben werden, welche Channel selektiert werden sollen.
Beispielsweise wählt :python:`{'openSMILE-features': ['pcm_RMSenergy_sma_amean']}` den Channel :python:`pcm_RMSenergy_sma_amean` im Stream :python:`openSMILE-features` aus.
Falls für einen Stream kein Channel spezifiziert wird, wird automatisch der erste (und häufig einzige) Channel ausgewählt.

Eine Normierung der Inputs ist durch Angabe der Variable :python:`STREAM_CHANNEL_MIN_MAX` möglich. Für jeden Stream und (optional) für jeden darin enthaltenen Channel muss hierfür das Minimum und Maximum des erwarteten Wertebereichs angegeben werden.
Das Beispiel :python:`{'openSMILE-features': {DEFAULT: (0.01, 0.15)}}` gibt an, dass im Stream :python:`openSMILE-features` alle Channels (:python:`DEFAULT`) im Wertebereich von 0.01 bis 0.15 liegen.
Wir kein Wertebereich angegeben, erfolgt keine Normierung (es empfiehlt sich deshalb im Allgemeinen, diesen Parameter zu nutzen).

Die Gewichtung der einzelnen Streams/Channels erfolgt durch Angabe von :python:`STREAM_CHANNEL_WEIGHT`. Es ist keine Voraussetzung, dass die einzelnen Gewichtungen in der Summe 1 ergeben, da automatisch aufsummiert und die Anteile berechnet werden.
Falls die Gewichtung für alle Channels in einem Stream gelten soll, kann auch hier :python:`DEFAULT` angegeben werden. Wird keine Gewichtung für einen Stream angegeben, wird automatisch mit 1 gewichtet.

arousal_fusion.py
-----------------

**Output-Stream**

- Sensor fusion: arousal

**Input-Streams**

- PhysiologyArousal (0)
    - Gewichtung: 1
- openSMILE-emotions (0)
    - Gewichtung: 1
- Orientation (ACC) vch.: 1 (Zero_Crossing)
    - Gewichtung: 1/3
- Orientation (ACC) vch.: 33 (Zero_Crossing)
    - Gewichtung: 1/3
- Orientation (ACC) vch.: 49 (Zero_Crossing)
    - Gewichtung: 1/3


aufmerksamkeit_fusion.py
------------------------

- MIN_DISTANCE_THRESHOLD
   - 400.0

**Output-Stream**

- Sensor fusion: aufmerksamkeit

**Input-Streams**

- eu.vtplus.vr.gaze_tracked_actors

Die Berechnung dieses Streams weicht von den anderen Fusionierungen ab.
Es wird hierfür ermittelt, welcher NPC, dessen Abstand unter dem festgelegten Schwellenwert MIN_DISTANCE_THRESHOLD liegt, mit dem kleinsten Winkel angeschaut wird.
Der Blickwinkel zu diesem NPC wird dann in einen Bereich von 0 bis 100 skaliert.

sicherheitsverhalten_fusion.py
------------------------------

**Output-Stream**

- Sensor fusion: sicherheitsverhalten

**Input-Streams**

- openSMILE-features (pcm_RMSenergy_sma_amean)

zitter_fusion
----------------

**Output-Stream**

- Sensor fusion: zittern

**Input-Streams**

- Orientation (ACC) vch.: 1 (Zero_Crossing)
    - Gewichtung: 1/3
- Orientation (ACC) vch.: 33 (Zero_Crossing)
    - Gewichtung: 1/3
- Orientation (ACC) vch.: 49 (Zero_Crossing)
    - Gewichtung: 1/3

Die Input Streams haben wir immer wieder angepasst, vermutlich werden sich die Zahlen auch ständig wieder ändern (?)

Allgemeine Erläuterungen zu Targetkonstrukten
=============================================

Der Algorithmus zur Klassifizierung der Targetkonstrukte variiert für die einzelnen Konstrukte.
An dieser Stelle finden sich deshalb die Konstanten, die in allen Konstrukten existieren.
Zusätzliche Details zu den jeweiligen Scripten finden sich in den jeweiligen Abschnitten.

:python:`DEFAULT_LSL_PREDICATE` spezifiziert das LSL-Predikat, das an das (frei verfügbare) Python-Package *pylsl* weitergereicht wird, um den Input-Streams zu selektieren.
Daber wird im Allgemeinen der Fusionsstream gelesen, der zur Vorhersage des Konstrukts genutzt wird.

Der Schwellwert, der vom Fusionsstream unter-/überschritten werden muss, damit das Konstrukt im Allgemeinen aktiviert wird, wird mit den Konstanten :python:`MIN_<Konstrukt>_THRESHOLD` angegeben.
Der Genaue Name dieser Konstante variiert je nach Targetkonstrukt.

targetconstruct_angst.py
------------------------

- MIN_ANGST_THRESHOLD
   - 70.0
- ANGST_MIN_DURATION
   - 0.5
- MIN_DURATION_FOR_RESET
   - 0.5

**Output-Stream**

- Target construct: Angst

**Input-Streams**

- Sensor fusion: arousal

Für dieses Konstrukt werden zusätzlich die Konstanten :python:`ANGST_MIN_DURATION` und :python:`MIN_DURATION_FOR_RESET` festgelegt.
Diese geben an, wie lange der Fusionsstream über/unter dem Schwellwert liegen muss, damit das Konstrukt aktiviert wird.

targetconstruct_aufmerksamkeit.py
---------------------------------

- ANSCHAU_SCHWELLWERT
   - 75.0
- ZEITSPANNE
   - 2.0
- BLICKKONTAKT_ZEITSPANNE
   - 2.0 * 0.9
- UNSICHERHEIT_ZEIT
   - 10

**Output-Stream**

- Target construct: Aufmerksamkeit

**Input-Streams**

- Sensor fusion: aufmerksamkeit

Als Blickkontakt wird gewertet, wenn in einem Zeitraum von :python:`ZEITSPANNE` Sekunden der Aufmerksamkeits-Wert aus dem fusion-aufmerksamkeit-stream für mindestens :python:`BLICKKONTAKT_ZEITSPANNE` Sekunden ueber dem Wert :python:`ANSCHAU_SCHWELLWERT` liegt.
Das Targetkonstrukt wird dann aktiviert, wenn der letzte Blickkontakt länger als :python:`UNSICHERHEIT_ZEIT` Sekunden her ist.

targetconstruct_sicherheitsverhalten.py
---------------------------------------

- MIN_SICHERHEIT_THRESHOLD
   - 20.0

**Output-Stream**

- Target construct: Sicherheitsverhalten

**Input-Streams**

- Sensor fusion: sicherheitsverhalten
   
Das Konstrukt wird aktiviert/deaktiviert, sobald der Schwellwert über/unterschritten ist.
