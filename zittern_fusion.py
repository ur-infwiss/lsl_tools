import argparse
import pylsl
import functools
from typing import Dict, List, Tuple
import lsl_stream_processor as lsl_sp
from helper_functions import get_git_revision_short_hash

DEFAULT = 'default'


class StreamFusion(lsl_sp.StreamProcessor):
    MAX_VAL = 100.
    MIN_VAL = 0.

    DEFAULT_LSL_PREDICATE: str = (
        # "(name ='PhysiologyArousal') "
        #"or name = 'openSMILE-emotions' "
        #"or contains(name, 'Orientation (Velocity) vch.: 18') "
        #"or contains(name, 'Orientation (Velocity) vch.: 34') "
        #"or contains(name, 'Orientation (Velocity) vch.: 50')"
        #"contains(name, 'Orientation (ACC) vch.: 1') "
        "contains(name, 'Orientation (ACC) vch.: 33') "
        "or contains(name, 'Orientation (ACC) vch.: 17') "
    )
    # "or contains(type, 'IMU')")
    # and starts-with(name,'BioSemi')
    #  and count(info/desc/channel)=32

    CHANNEL_FILTER: Dict[str, List[str]] = {
        'Orientation (ACC) vch.: 1': ['Zero_Crossing'],
        'Orientation (ACC) vch.: 33': ['Zero_Crossing'],
        'Orientation (ACC) vch.: 17': ['Zero_Crossing'],
        #'Orientation (Velocity) vch.: 18': ['Zero Crossing'],
        #'Orientation (Velocity) vch.: 34': ['Zero Crossing'],
        #'Orientation (Velocity) vch.: 50': ['Zero Crossing'],
    }

    STREAM_CHANNEL_MIN_MAX: Dict[str, Dict[str, Tuple[float, float]]] = {
        #'PhysiologyArousal': {
        #    DEFAULT: (0., 100.)},
        #'openSMILE-emotions': {
        #    DEFAULT: (-1., 1.)},
        #'Orientation (Velocity) vch.: 18': {
        #    'Zero Crossing': (0., 8.)},
        #'Orientation (Velocity) vch.: 34': {
        #    'Zero Crossing': (0., 8.)},
        #'Orientation (Velocity) vch.: 50': {
        #    'Zero Crossing': (0., 8.)},
        'Orientation (ACC) vch.: 1': {  # diesen IMU bitte rauswerfen, ist am Körper
            'Zero_Crossing': (0., 15.)},
        'Orientation (ACC) vch.: 33': {
            'Zero_Crossing': (0., 15.)}, # Grenze bei 8 scheint zu niedrig, bitte checken
        'Orientation (ACC) vch.: 17': {
            'Zero_Crossing': (0., 15.)},

    }

    STREAM_CHANNEL_WEIGHT: Dict[str, Dict[str, float]] = {
        #'PhysiologyArousal': {
        #    DEFAULT: 1.},
        #'openSMILE-emotions': {
        #    DEFAULT: 1.},
        'Orientation (Velocity) vch.: 18': {  # hier stehen wieder die falschen Namen
            'Zero_Crossing': 1. / 3.},
        'Orientation (Velocity) vch.: 34': {
            'Zero_Crossing': 1. / 3.},
        'Orientation (Velocity) vch.: 50': {
            'Zero_Crossing': 1. / 3.},
    }

    def __init__(self, inlets_predicate: str, min_inlets: int) -> None:
        streams = pylsl.resolve_stream()
        if streams:
            print("Currently available lsl streams:")
            for stream in streams:
                available_stream = ("name: {}, type: {}, number of channels: {}".
                                    format(stream.name(), stream.type(),
                                           stream.channel_count()))
                print(available_stream)
        else:
            print("At the moment no lsl stream could be found.")

        
        outlet_info: pylsl.StreamInfo = pylsl.StreamInfo(
            "Sensor fusion: zittern", "fused zittern", 1, 0, pylsl.cf_float32,
            source_id="git_short_hash=zittern_" + get_git_revision_short_hash())
        outlet_info.desc().append_child_value("manufacturer",
                                              "UR Informationswissenschaft")
        channels = outlet_info.desc().append_child("channels")
        angst_channel = channels.append_child("channel")
        angst_channel.append_child_value("label", "average zittern")
        # Maybe we should provide these as parameters to init?
        self.channel_filter_ = StreamFusion.CHANNEL_FILTER
        self.stream_channel_min_max_ = StreamFusion.STREAM_CHANNEL_MIN_MAX
        self.stream_channel_weight_ = StreamFusion.STREAM_CHANNEL_WEIGHT
        super().__init__(outlet_info, inlets_predicate, min_inlets)

    @functools.lru_cache()
    def get_channel_list(
            self, inlet_info: pylsl.StreamInfo) -> Dict[str, int]:
        if inlet_info.name() not in self.channel_filter_:
            return {DEFAULT: 0}
        else:
            channel_labels = self.channel_filter_[inlet_info.name()]
            channel_lookup_table = {}
            for channel_label in channel_labels:
                channel_number = lsl_sp.get_channel_by_label(inlet_info,
                                                             channel_label)
                channel_lookup_table[channel_label] = channel_number
            return channel_lookup_table

    def normalize(self, stream_name: str, channel_label: str,
                  value: float) -> float:
        min_value, max_value = None, None
        if stream_name in self.stream_channel_min_max_:
            channel_min_max = self.stream_channel_min_max_[stream_name]
            if channel_label in channel_min_max:
                min_value, max_value = channel_min_max[channel_label]
            elif DEFAULT in channel_min_max:
                min_value, max_value = channel_min_max[DEFAULT]
        if min_value is not None:
            if value < min_value:
                print("Stream {}, Channel {} sending {}. This is smaller than the minimum value ({}).".format(strea_name, channel_label, value, min_value))
                value = min_value
            if value > max_value:
                print("Stream {}, Channel {} sending {}. This exceeds the maximum value ({}).".format(stream_name, channel_label, value, max_value))
                value = max_value
            value -= min_value
            value /= (max_value - min_value)
        return value

    @functools.lru_cache()
    def get_weight_factor(self, stream_name: str, channel_label: str) -> float:
        weight = 1.
        if stream_name in self.stream_channel_weight_:
            channel_min_max = self.stream_channel_weight_[stream_name]
            if channel_label in channel_min_max:
                weight = channel_min_max[channel_label]
            elif DEFAULT in channel_min_max:
                weight = channel_min_max[DEFAULT]
        return weight

    def output_loop(self) -> None:
        newest_values: Dict[Tuple[str, str], float] = {}
        #print("1")
        while True:
            input_value: lsl_sp.StreamProcessor.INPUT_QUEUE_VALUE_TYPE
            input_value = self.input_queue.get()  # blocking read
            #print("2")
            inlet, sample, timestamp = input_value
            corrected_timestamp = timestamp # + inlet.time_correction()
            # the actual computation
            channel_lookup: str = self.get_channel_list(inlet.info())
            stream_name: str = inlet.info().name()
            for channel_label, channel_number in channel_lookup.items():
                stream_channel_id = (stream_name, channel_label)
                new_value = sample[channel_number]
                new_value = self.normalize(stream_name,
                                           channel_label,
                                           new_value)
                newest_values[stream_channel_id] = new_value
                #print("Stream: {}, Value: {}".format(stream_name, new_value))
            value_sum = 0
            weight_sum = 0
            for (strea_name, channel_label), value in newest_values.items():
                weight = self.get_weight_factor(strea_name, channel_label)
                value_sum += weight * value
                weight_sum += weight
            average = value_sum / float(weight_sum)
            average = average * (StreamFusion.MAX_VAL - StreamFusion.MIN_VAL) + StreamFusion.MIN_VAL
            #if average > ArousalFusion.MAX_VAL:
            #    average = ArousalFusion.MAX_VAL
            #if average < ArousalFusion.MIN_VAL:
            #    average = ArousalFusion.MIN_VAL
            print(corrected_timestamp, average)
            self.outlet.push_sample([average], corrected_timestamp)


DEFAULT_MIN_NUMBER_OF_STREAMS = 1
parser = argparse.ArgumentParser(
    description='Computes the average of multiple streams.')
parser.add_argument("--min_number_of_streams", "-n",
                    dest="min_number_of_streams", type=int,
                    default=DEFAULT_MIN_NUMBER_OF_STREAMS,
                    help="Wait for at least this number of streams to be "
                         "available "
                         "before outputting their average, default: %d" %
                         DEFAULT_MIN_NUMBER_OF_STREAMS)
parser.add_argument("--predicate", "-p", dest='predicate', type=str,
                    default=StreamFusion.DEFAULT_LSL_PREDICATE,
                    help="The predicate that selects all relevant streams to "
                         "be averaged ."
                         "Default: {}".format(
                        StreamFusion.DEFAULT_LSL_PREDICATE))
args = parser.parse_args()
#print(vars(args))

#print("git_short_hash=" + get_git_revision_short_hash())

arousal_fusion = StreamFusion(args.predicate, args.min_number_of_streams)
arousal_fusion.output_loop()
