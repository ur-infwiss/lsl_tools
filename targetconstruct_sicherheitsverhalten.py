import argparse
import pylsl
import queue
from lsl_stream_processor import StreamProcessor
import logging
from helper_functions import get_git_revision_short_hash


logging.basicConfig(level=logging.INFO)


class TargetConstructSicherheitsverhalten(StreamProcessor):

    DEFAULT_LSL_PREDICATE: str = "type='fused sicherheitsverhalten'"

    MIN_SICHERHEIT_THRESHOLD: float = 20.0
    #  SICHERHEIT_MIN_DURATION: float = .5
    #  MIN_DURATION_FOR_RESET: float = .5

    def __init__(self, inlets_predicate: str) -> None:
        outlet_info = pylsl.StreamInfo(
            "Target construct: Sicherheitsverhalten", "event markers", 1, 0, pylsl.cf_string,
            source_id="git_short_hash=target_sicher_" + get_git_revision_short_hash())
        outlet_info.desc().append_child_value("manufacturer", "UR Informationswissenschaft")
        channels = outlet_info.desc().append_child("channels")
        sicherheit_channel = channels.append_child("channel")
        sicherheit_channel.append_child_value("label", "target construct: sicherheitsverhalten")
        sicherheit_channel.append_child_value("type", "event markers")
        sicherheit_channel.append_child_value("MIN_SICHERHEITSVERHALTEN_THRESHOLD", str(TargetConstructSicherheitsverhalten.MIN_SICHERHEIT_THRESHOLD))
        #  sicherheit_channel.append_child_value("SICHERHEITSVERHALTEN_MIN_DURATION", str(TargetConstructSicherheitsverhalten.SICHERHEIT_MIN_DURATION))
        #  sicherheit_channel.append_child_value("MIN_DURATION_FOR_RESET", str(TargetConstructSicherheitsverhalten.MIN_DURATION_FOR_RESET))

        super().__init__(outlet_info, inlets_predicate)

    def output_loop(self) -> None:
        last_sicherheitsverhalten: float = 100.
        timestamp_of_threshold_crossing: float = 0.
        unsicherheit_detected: bool = False

        while True:
            try:
                input_value: StreamProcessor.INPUT_QUEUE_VALUE_TYPE = self.input_queue.get(block=True, timeout=1.0)
                _, sample, timestamp = input_value

                current_sicherheitsverhalten: float = sample[0]

                threshold_crossed: bool = (
                        last_sicherheitsverhalten < TargetConstructSicherheitsverhalten.MIN_SICHERHEIT_THRESHOLD <= current_sicherheitsverhalten
                        or last_sicherheitsverhalten >= TargetConstructSicherheitsverhalten.MIN_SICHERHEIT_THRESHOLD > current_sicherheitsverhalten)
                if threshold_crossed:
                    timestamp_of_threshold_crossing = timestamp
                    if current_sicherheitsverhalten < TargetConstructSicherheitsverhalten.MIN_SICHERHEIT_THRESHOLD:
                        print(timestamp_of_threshold_crossing, "Unsicherheit detected, sending marker")
                        logging.info("%s: Unsicherheit detected, sending marker.", timestamp_of_threshold_crossing)
                        self.outlet.push_sample(["true"], timestamp_of_threshold_crossing)
                    else:
                        print(timestamp_of_threshold_crossing, "Unsicherheit detected no more, sending marker")
                        logging.info("%s: Unsicherheit detected no more, sending marker.", timestamp_of_threshold_crossing)
                        self.outlet.push_sample(["false"], timestamp_of_threshold_crossing)

                last_sicherheitsverhalten = current_sicherheitsverhalten

            except queue.Empty:
                # time has elapsed without new input, now check our timeouts
                pass

            """
            timestamp: float = pylsl.local_clock()

            if (last_sicherheitsverhalten >= TargetConstructSicherheitsverhalten.MIN_SICHERHEIT_THRESHOLD
                    and timestamp_of_threshold_crossing + TargetConstructSicherheitsverhalten.SICHERHEIT_MIN_DURATION <= timestamp):
                # angst detected
                if not unsicherheit_detected:
                    unsicherheit_detected = True
                    timestamp_of_angst_detection = (timestamp_of_threshold_crossing +
                                                    TargetConstructSicherheitsverhalten.SICHERHEIT_MIN_DURATION)
                    print(timestamp_of_angst_detection, "Unsicherheit detected, sending marker")
                    logging.info("%s: Unsicherheit detected, sending marker.", timestamp_of_angst_detection)
                    self.outlet.push_sample(["unsicherheit"], timestamp_of_angst_detection)

            if (last_sicherheitsverhalten < TargetConstructSicherheitsverhalten.MIN_SICHERHEIT_THRESHOLD
                    and timestamp_of_threshold_crossing + TargetConstructSicherheitsverhalten.MIN_DURATION_FOR_RESET <= timestamp):
                # no more angst
                if unsicherheit_detected:
                    unsicherheit_detected = False
                    timestamp_of_no_angst_detection = (timestamp_of_threshold_crossing +
                                                       TargetConstructSicherheitsverhalten.MIN_DURATION_FOR_RESET)
                    print(timestamp_of_no_angst_detection, "no more angst")
                    logging.info("%s: No more Unsicherheit detected, sending marker.", timestamp_of_no_angst_detection)
                    self.outlet.push_sample(["no more unsicherheit"], timestamp_of_no_angst_detection)
            """

parser = argparse.ArgumentParser(description='Detects the target construct Sicherheit based on the "fused sicherheitsverhalten" stream.')
parser.add_argument("--predicate", "-p", dest='predicate', type=str,
                    default=TargetConstructSicherheitsverhalten.DEFAULT_LSL_PREDICATE,
                    help="The predicate that selects the relevant input stream."
                         "Default: {}".format(TargetConstructSicherheitsverhalten.DEFAULT_LSL_PREDICATE))
args = parser.parse_args()
# print(vars(args))

target_construct_sicherheitsverhalten = TargetConstructSicherheitsverhalten(args.predicate)
target_construct_sicherheitsverhalten.output_loop()
