import argparse
import pylsl
import queue
from lsl_stream_processor import StreamProcessor
import logging
from helper_functions import get_git_revision_short_hash


logging.basicConfig(level=logging.INFO)


class TargetConstructAngst(StreamProcessor):

    DEFAULT_LSL_PREDICATE: str = "type='fused arousal'"

    MIN_ANGST_THRESHOLD: float = 70.0
    ANGST_MIN_DURATION: float = .5
    MIN_DURATION_FOR_RESET: float = .5

    def __init__(self, inlets_predicate: str) -> None:
        outlet_info = pylsl.StreamInfo(
            "Target construct: Angst", "event markers", 1, 0, pylsl.cf_string,
            source_id="git_short_hash=target_angst_" + get_git_revision_short_hash())
        outlet_info.desc().append_child_value("manufacturer", "UR Informationswissenschaft")
        channels = outlet_info.desc().append_child("channels")
        angst_channel = channels.append_child("channel")
        angst_channel.append_child_value("label", "target construct: angst")
        angst_channel.append_child_value("type", "event markers")
        angst_channel.append_child_value("MIN_SICHERHEIT_THRESHOLD", str(TargetConstructAngst.MIN_ANGST_THRESHOLD))
        angst_channel.append_child_value("SICHERHEIT_MIN_DURATION", str(TargetConstructAngst.ANGST_MIN_DURATION))
        angst_channel.append_child_value("MIN_DURATION_FOR_RESET", str(TargetConstructAngst.MIN_DURATION_FOR_RESET))

        super().__init__(outlet_info, inlets_predicate)

    def output_loop(self) -> None:
        last_arousal: float = 0.
        timestamp_of_threshold_crossing: float = 0.
        angst_detected: bool = False

        while True:
            try:
                input_value: StreamProcessor.INPUT_QUEUE_VALUE_TYPE = self.input_queue.get(block=True, timeout=1.0)
                _, sample, timestamp = input_value

                current_arousal: float = sample[0]

                threshold_crossed: bool = (
                        last_arousal < TargetConstructAngst.MIN_ANGST_THRESHOLD <= current_arousal
                        or last_arousal >= TargetConstructAngst.MIN_ANGST_THRESHOLD > current_arousal)
                if threshold_crossed:
                    timestamp_of_threshold_crossing = timestamp

                last_arousal = current_arousal

            except queue.Empty:
                # time has elapsed without new input, now check our timeouts
                pass

            timestamp: float = pylsl.local_clock()

            if (last_arousal >= TargetConstructAngst.MIN_ANGST_THRESHOLD
                    and timestamp_of_threshold_crossing + TargetConstructAngst.ANGST_MIN_DURATION <= timestamp):
                # angst detected
                if not angst_detected:
                    angst_detected = True
                    timestamp_of_angst_detection = (timestamp_of_threshold_crossing +
                                                    TargetConstructAngst.ANGST_MIN_DURATION)
                    print(timestamp_of_angst_detection, "angst detected, sending marker")
                    logging.info("%s: Angst detected, sending marker.", timestamp_of_angst_detection)
                    self.outlet.push_sample(["true"], timestamp_of_angst_detection)

            if (last_arousal < TargetConstructAngst.MIN_ANGST_THRESHOLD
                    and timestamp_of_threshold_crossing + TargetConstructAngst.MIN_DURATION_FOR_RESET <= timestamp):
                # no more angst
                if angst_detected:
                    angst_detected = False
                    timestamp_of_no_angst_detection = (timestamp_of_threshold_crossing +
                                                       TargetConstructAngst.MIN_DURATION_FOR_RESET)
                    print(timestamp_of_no_angst_detection, "no more angst")
                    logging.info("%s: No more Angst detected, sending marker.", timestamp_of_no_angst_detection)
                    self.outlet.push_sample(["false"], timestamp_of_no_angst_detection)


parser = argparse.ArgumentParser(description='Detects the target construct angst based on the "fused arousal" stream.')
parser.add_argument("--predicate", "-p", dest='predicate', type=str,
                    default=TargetConstructAngst.DEFAULT_LSL_PREDICATE,
                    help="The predicate that selects the relevant input stream."
                         "Default: {}".format(TargetConstructAngst.DEFAULT_LSL_PREDICATE))
args = parser.parse_args()
# print(vars(args))

target_construct_angst = TargetConstructAngst(args.predicate)
target_construct_angst.output_loop()
