import numpy as np
import quaternion
import pandas as pd


def _calc_features(feature_data_raw: np.ndarray):
    features = []
    feature_labels = []

    # quaternion.as_quat_array()
    # print(len(feature_data_raw))
    max_length = 200
    if len(feature_data_raw) < max_length:
        raise Exception("max_length too small to fit all data")
    feature_data_raw = feature_data_raw[
                       int((len(feature_data_raw) - max_length) / 2):int(
                           (len(feature_data_raw) + max_length) / 2)]

    """
    windows = np.array_split(feature_data_raw,10)
    feature_labels = []
    for i, w in enumerate(windows):
        features.append(w.mean())
        feature_labels.append("Mean of {}st window".format(i))
        features.append(w.std())
        feature_labels.append("Std of {}st window".format(i))
        #features.append(np.median(w))
        #feature_labels.append("Median of {}st window".format(i))
    """

    # print(len(feature_data_raw))
    # print(feature_data_raw)

    """
    data_ft = np.fft.rfft(feature_data_raw)
    feature_labels = np.fft.rfftfreq(len(feature_data_raw), d=1.0 / 200) 
    #wrong samplerate, it's not 200
    features = np.absolute(data_ft).tolist()
    feature_labels = feature_labels.tolist()
    """

    features.append(feature_data_raw.mean())
    features.append(feature_data_raw.std())
    feature_labels += ["mean", "std"]

    # print (features)

    return features, feature_labels


def add_quaternion_columns(table):
    quat = np.array(table[["X", "Y", "Z", "W"]])
    quat = quaternion.as_quat_array(quat)
    # quat = quaternion.from_spherical_coords(quat)

    euler_angles = (quaternion.as_rotation_vector(quat))
    # euler_angles = (quaternion.as_euler_angles(quat))
    euler_cos = np.cos(euler_angles)
    euler_sin = np.sin(euler_angles)
    cos_names = ["cosX", "cosY", "cosZ"]
    for i, name in enumerate(cos_names):
        if len(euler_cos.shape) == 1:
            table[name] = pd.Series(euler_cos[i], index=table.index)
        else:
            table[name] = pd.Series(euler_cos[:, i], index=table.index)
    sin_names = ["sinX", "sinY", "sinZ"]
    for i, name in enumerate(sin_names):
        if len(euler_sin.shape) == 1:
            table[name] = pd.Series(euler_sin[i], index=table.index)
        else:
            table[name] = pd.Series(euler_sin[:, i], index=table.index)
    column_names = cos_names + sin_names
    return column_names


def return_quaternions(xyzw):
    quat = np.array([xyzw["X"], xyzw["Y"], xyzw["Z"], xyzw["W"]])
    quat = quaternion.as_quat_array(quat)
    euler_angles = (quaternion.as_rotation_vector(quat))
    euler_cos = np.cos(euler_angles)
    euler_sin = np.sin(euler_angles)
    cos_names = ["cosX", "cosY", "cosZ"]
    return_dict = {}
    for i, name in enumerate(cos_names):
        if len(euler_cos.shape) == 1:
            return_dict[name] = euler_cos[i]
        else:
            return_dict[name] = euler_cos[:, i]
    sin_names = ["sinX", "sinY", "sinZ"]
    for i, name in enumerate(sin_names):
        if len(euler_sin.shape) == 1:
            return_dict[name] = euler_sin[i]
        else:
            return_dict[name] = euler_sin[:, i]
    return return_dict
