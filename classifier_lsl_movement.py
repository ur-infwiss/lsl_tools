import argparse
import pylsl
import queue
import lsl_stream_processor
import pandas as pd
import numpy as np
from typing import List, Dict
import joblib
from lsl_errors import *
import classifier_config
import classifier_data_processing

REPORT_PROBABILITY_THRESHOLD = 0.4


class ChannelDataWindow:
    def __init__(self, channel_labels, window_size):
        self.data = pd.DataFrame(np.zeros((window_size, len(channel_labels))),
                                 columns=channel_labels, dtype="float32")
        self.window_size = window_size
        self.channel_labels = channel_labels

    def append(self, sample):
        quaternion_data = classifier_data_processing.return_quaternions(sample)
        sample.update(quaternion_data)
        self.data = self.data.shift(periods=-1)
        self.data.iloc[-1] = sample

    def get_window(self):
        return self.data


class StreamDataCollector:
    def __init__(self,
                 inlet_info: pylsl.StreamInfo,
                 full_channel_labels: List[str],
                 channel_labels: List[str],
                 window_size: int
                 ):
        self.channel_labels = channel_labels
        self.name = inlet_info.name()
        self.channel_label_to_number: Dict[str, int] = {}
        self.channel_data: ChannelDataWindow = ChannelDataWindow(
            full_channel_labels,
            window_size)
        self.window_size = window_size
        for channel_label in channel_labels:
            channel_number = lsl_stream_processor.get_channel_by_label(
                inlet_info,
                channel_label)
            self.channel_label_to_number[channel_label] = channel_number

    def append_stream_sample(self,
                             sample):
        sample_data = {}
        for channel_label in self.channel_labels:
            channel_number = self.channel_label_to_number[channel_label]
            sample_data[channel_label] = sample[channel_number]
        self.channel_data.append(sample_data)

    def get_table(self):
        table = self.channel_data.get_window()
        return table


class MovementClassifier(lsl_stream_processor.StreamProcessor):
    CLASSIFIER_NAME = "RandomForestClassifier"

    def __init__(self,
                 inlets_predicate: str) -> None:
        self.channel_labels = []
        self.stream_names = []
        self.read_feature_list()
        outlet_info = pylsl.StreamInfo(
            "Recognized movements", "event markers", 1, 0, pylsl.cf_string)
        outlet_info.desc().append_child_value("manufacturer",
                                              "UR Informationswissenschaft")
        channels = outlet_info.desc().append_child("channels")
        output_channel = channels.append_child("channel")
        output_channel.append_child_value("label",
                                          "Recognized movement classes")
        output_channel.append_child_value("type", "event markers")
        self.classifier = self.load_classifier()
        super().__init__(outlet_info, inlets_predicate, 3)

    def load_classifier(self):
        return joblib.load(self.CLASSIFIER_NAME + '.pkl')

    def read_feature_list(self):
        filename = classifier_config.FEATURE_LABELS_FILENAME
        with open(filename, "r") as feature_file:
            for line in feature_file.read().splitlines():
                line = line.split("~")
                stream_name = line[0]
                channel_name = line[1]
                if stream_name not in self.stream_names:
                    self.stream_names.append(stream_name)
                if channel_name not in self.channel_labels:
                    self.channel_labels.append(channel_name)

    def output_loop(self) -> None:
        stream_collectors: Dict[str, StreamDataCollector] = {}
        got_new_data_for_streams = set()
        while True:
            try:
                input_value: \
                    lsl_stream_processor.StreamProcessor \
                        .INPUT_QUEUE_VALUE_TYPE = self.input_queue.get(
                    block=True, timeout=1.0)
                inlet, sample, timestamp = input_value
                inlet_info = inlet.info()
                inlet_name = inlet_info.name()
                if inlet_name not in stream_collectors:
                    stream_collectors[inlet_name] = StreamDataCollector(
                        inlet_info,
                        self.channel_labels,
                        classifier_config.CHANNEL_LABELS,
                        classifier_config.WINDOW_SIZE
                    )
                stream_collectors[inlet_name].append_stream_sample(sample)
                got_new_data_for_streams.add(inlet_name)
                if got_new_data_for_streams == set(self.stream_names):
                    got_new_data_for_streams = set()
                    feature_vector = []
                    for stream_name in self.stream_names:
                        # print(stream_collectors.keys())
                        # print(stream_collectors['Orientation (Velocity)
                        # vch.: 18'].get_table())
                        stream_table = stream_collectors[
                            stream_name].get_table()
                        if classifier_config.WINDOW_SIZE != stream_table.shape[
                            0]:
                            break
                        # print(stream_table.to_string())
                        for channel_label in self.channel_labels:
                            one_feature_timeframe = np.array(
                                stream_table[channel_label])
                            features, feature_labels = \
                                classifier_data_processing._calc_features(
                                    one_feature_timeframe)
                            for feature in features:
                                feature_vector.append(feature)
                    else:
                        classes = self.classify(feature_vector)
                        if classes is not None:
                            print(classes)
                            timestamp: float = pylsl.local_clock()
                            self.outlet.push_sample([classes, ], timestamp)
            except queue.Empty:
                print("no data")
                # time has elapsed without new input, now check our timeouts

    def classify(self, feature_vector):
        probabilities = self.classifier.predict_proba(
            np.array(feature_vector).reshape(1, -1))[0]
        predictions = zip(self.classifier.classes_, probabilities)
        max_likelihood = max(predictions, key=lambda x: x[1])
        if max_likelihood[1] < REPORT_PROBABILITY_THRESHOLD:
            return None
        else:
            return max_likelihood[0]


"""
parser = argparse.ArgumentParser(description='Detects the target construct 
angst based on the "fused arousal" stream.')
parser.add_argument("--predicate", "-p", dest='predicate', type=str,
                    default=MovementClassifier.DEFAULT_LSL_PREDICATE,
                    help="The predicate that selects the relevant input stream."
                         "Default: {}".format(
                         MovementClassifier.DEFAULT_LSL_PREDICATE))
args = parser.parse_args()
print(vars(args))
"""


def stream_predicate_to_string(stream_predicate):
    stream_predicates_list = []
    for predicate_item in classifier_config.STREAM_PREDICATE.items():
        stream_predicates_list.append("{}='{}'".format(*predicate_item))
    stream_predicate_string = " and ".join(stream_predicates_list)
    return stream_predicate_string


stream_predicate = classifier_config.STREAM_PREDICATE
stream_predicate_string = stream_predicate_to_string(stream_predicate)
print(stream_predicate_string)
target_construct_angst = MovementClassifier(stream_predicate_string)
target_construct_angst.output_loop()
