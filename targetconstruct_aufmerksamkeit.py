import argparse
import pylsl
import queue
from lsl_stream_processor import StreamProcessor
import logging
from helper_functions import get_git_revision_short_hash


logging.basicConfig(level=logging.INFO)


class TargetConstructAufmerksamkeit(StreamProcessor):

    DEFAULT_LSL_PREDICATE: str = "type='fused aufmerksamkeit'"

    #  Als Blickkontakt wird gewertet, wenn in einem
    #  Zeitraum von *ZEITSPANNE*  Sekunden der Aufmerksamkeits-Wert
    #  aus dem fusion-aufmerksamkeit-stream fuer
    #  mindestens *BLICKKONTAKT_ZEITSPANNE* Sekunden ueber dem
    #  Wert *ANSCHAU_SCHWELLWERT* liegt.
    ANSCHAU_SCHWELLWERT = 75.0
    ZEITSPANNE = 2.0
    # 90% der Zeitspanne
    BLICKKONTAKT_ZEITSPANNE = 2.0 * 0.9

    #  Das Targetkonstrukt soll feuern, wenn der letzte Blickkontakt laenger
    #  als *unsicherheit_zeit* Sekunden her ist.
    UNSICHERHEIT_ZEIT = 10


    def __init__(self, inlets_predicate: str) -> None:
        outlet_info = pylsl.StreamInfo(
            "Target construct: Aufmerksamkeit", "event markers", 1, 0, pylsl.cf_string,
            source_id="git_short_hash=target_aufmerk_" + get_git_revision_short_hash())
        outlet_info.desc().append_child_value("manufacturer", "UR Informationswissenschaft")
        channels = outlet_info.desc().append_child("channels")
        angst_channel = channels.append_child("channel")
        angst_channel.append_child_value("label", "target construct: aufmerksamkeit")
        angst_channel.append_child_value("type", "event markers")
        #angst_channel.append_child_value("MIN_AUFMERKSAMKEIT_THRESHOLD", str(TargetConstructAufmerksamkeit.MIN_AUFMERKSAMKEIT_THRESHOLD))
        #angst_channel.append_child_value("AUFMERKSAMKEIT_MIN_DURATION", str(TargetConstructAufmerksamkeit.AUFMERKSAMKEIT_MIN_DURATION))
        #angst_channel.append_child_value("MIN_DURATION_FOR_RESET", str(TargetConstructAufmerksamkeit.MIN_DURATION_FOR_RESET))

        super().__init__(outlet_info, inlets_predicate)

    def output_loop(self) -> None:
        # last_arousal: float = 0.
        # timestamp_of_threshold_crossing: float = 0.
        # angst-_detected: bool = False



        zeit_letzter_blickkontakt = None
        zeit_letzter_sample = None
        blicke_der_letzten_sekunden = []
        target_ist_aktiv = False
        
        while True:
            try:
                input_value: StreamProcessor.INPUT_QUEUE_VALUE_TYPE = self.input_queue.get(block=True, timeout=1.0)
                stream_inlet, sample, timestamp = input_value

                current_arousal: float = sample[0]


                if zeit_letzter_blickkontakt is None:
                    #  Werte initialisieren
                    zeit_letzter_blickkontakt = timestamp
                elif current_arousal>TargetConstructAufmerksamkeit.ANSCHAU_SCHWELLWERT:
                    #  Falls Nutzer in die Augen schaut
                    blicke_der_letzten_sekunden.append({
                        "zeitpunkt": zeit_letzter_sample,
                        "dauer": timestamp - zeit_letzter_sample #  Als Naeherungswert wird die Zeit
                                                                 #  vor dem Zeitstempel herangezogen
                    })
                blicke_der_letzten_sekunden = list(filter(lambda x: x["zeitpunkt"] > timestamp - TargetConstructAufmerksamkeit.ZEITSPANNE,
                                                          blicke_der_letzten_sekunden))
                zeit_mit_blicken = sum([x["dauer"] for x in blicke_der_letzten_sekunden])
                #  print(zeit_mit_blicken, TargetConstructAufmerksamkeit.BLICKKONTAKT_ZEITSPANNE)
                if zeit_mit_blicken > TargetConstructAufmerksamkeit.BLICKKONTAKT_ZEITSPANNE:
                    #  print("aufmerksam")
                    zeit_letzter_blickkontakt = timestamp
                    if target_ist_aktiv:
                        target_ist_aktiv = False
                        timestamp_of_angst_detection = zeit_letzter_blickkontakt
                        print(timestamp_of_angst_detection, "Aufmerksamkeit detected, sending marker")
                        logging.info("%s: Aufmerksamkeit detected, sending marker.", timestamp_of_angst_detection)
                        self.outlet.push_sample(["false"], timestamp_of_angst_detection)
                if zeit_letzter_blickkontakt < timestamp - TargetConstructAufmerksamkeit.UNSICHERHEIT_ZEIT:
                    if not target_ist_aktiv:
                        target_ist_aktiv = True
                        timestamp_of_angst_detection = zeit_letzter_blickkontakt + TargetConstructAufmerksamkeit.UNSICHERHEIT_ZEIT
                        print(timestamp_of_angst_detection, "Unaufmerksamkeit detected, sending marker")
                        logging.info("%s: Unaufmerksamkeit detected, sending marker.", timestamp_of_angst_detection)
                        self.outlet.push_sample(["true"], timestamp_of_angst_detection)
                zeit_letzter_sample = timestamp


                """
                threshold_crossed: bool = (
                        last_arousal < TargetConstructAufmerksamkeit.MIN_AUFMERKSAMKEIT_THRESHOLD <= current_arousal
                        or last_arousal >= TargetConstructAufmerksamkeit.MIN_AUFMERKSAMKEIT_THRESHOLD > current_arousal)
                if threshold_crossed:
                    # print(stream_inlet.time_correction())
                    timestamp_of_threshold_crossing = timestamp # + stream_inlet.time_correction()

                last_arousal = current_arousal
                """
            except queue.Empty:
                # time has elapsed without new input, now check our timeouts
                pass

            timestamp: float = pylsl.local_clock()


            """
            if (last_arousal <= TargetConstructAufmerksamkeit.MIN_AUFMERKSAMKEIT_THRESHOLD
                    and timestamp_of_threshold_crossing + TargetConstructAufmerksamkeit.AUFMERKSAMKEIT_MIN_DURATION <= timestamp):
                # angst detected
                if not angst_detected:
                    angst_detected = True
                    timestamp_of_angst_detection = (timestamp_of_threshold_crossing +
                                                    TargetConstructAufmerksamkeit.AUFMERKSAMKEIT_MIN_DURATION)
                    print(timestamp_of_angst_detection, "Unaufmerksamkeit detected, sending marker")
                    logging.info("%s: Unaufmerksamkeit detected, sending marker.", timestamp_of_angst_detection)
                    self.outlet.push_sample(["true"], timestamp_of_angst_detection)
            # print(last_arousal, TargetConstructAufmerksamkeit.MIN_AUFMERKSAMKEIT_THRESHOLD)
            # print(timestamp_of_threshold_crossing, TargetConstructAufmerksamkeit.MIN_DURATION_FOR_RESET, timestamp)
            if (last_arousal > TargetConstructAufmerksamkeit.MIN_AUFMERKSAMKEIT_THRESHOLD
                    and timestamp_of_threshold_crossing + TargetConstructAufmerksamkeit.MIN_DURATION_FOR_RESET <= timestamp):
                # no more angst
                if angst_detected:
                    angst_detected = False
                    timestamp_of_no_angst_detection = (timestamp_of_threshold_crossing +
                                                       TargetConstructAufmerksamkeit.MIN_DURATION_FOR_RESET)
                    print(timestamp_of_no_angst_detection, "no more Unaufmerksamkeit")
                    logging.info("%s: No more Unaufmerksamkeit detected, sending marker.", timestamp_of_no_angst_detection)
                    self.outlet.push_sample(["false"], timestamp_of_no_angst_detection)
            """

parser = argparse.ArgumentParser(description='Detects the target construct aufberksamkeit based on the "fused aufmerksamkeit" stream.')
parser.add_argument("--predicate", "-p", dest='predicate', type=str,
                    default=TargetConstructAufmerksamkeit.DEFAULT_LSL_PREDICATE,
                    help="The predicate that selects the relevant input stream."
                         "Default: {}".format(TargetConstructAufmerksamkeit.DEFAULT_LSL_PREDICATE))
args = parser.parse_args()
# print(vars(args))

target_construct_angst = TargetConstructAufmerksamkeit(args.predicate)
target_construct_angst.output_loop()
