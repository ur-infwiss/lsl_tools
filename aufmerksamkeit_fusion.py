import argparse
import pylsl
import functools
from typing import Dict, List, Tuple
import lsl_stream_processor as lsl_sp
import xmltodict
from helper_functions import get_git_revision_short_hash

DEFAULT = 'default'


class SicherheitsverhaltenFusion(lsl_sp.StreamProcessor):
    MAX_VAL = 100.
    MIN_VAL = 0.
    # Blickkontakt, wenn in einem 2 sec Fenster 90% der Werte ueber 90 sind
    # Targetkonstrukt feuert, wenn in den letzten 10 sec nie Blickkontakt
    # 
    DEFAULT_LSL_PREDICATE: str = (
        "name = 'eu.vtplus.vr.gaze_tracked_actors'"
    )

    def __init__(self, inlets_predicate: str, min_inlets: int) -> None:
        outlet_info: pylsl.StreamInfo = pylsl.StreamInfo(
            "Sensor fusion: aufmerksamkeit", "fused aufmerksamkeit", 1, 0, pylsl.cf_float32,
            source_id="git_short_hash=aufmerk_" + get_git_revision_short_hash())
        outlet_info.desc().append_child_value("manufacturer",
                                              "UR Informationswissenschaft")
        channels = outlet_info.desc().append_child("channels")
        angst_channel = channels.append_child("channel")
        angst_channel.append_child_value("label", "average aufmerksamkeit")
        # Maybe we should provide these as parameters to init?
        super().__init__(outlet_info, inlets_predicate, min_inlets)

    def output_loop(self) -> None:
        newest_values: Dict[Tuple[str, str], float] = {}
        MIN_DISTANCE_THRESHOLD = 200.0
        while True:
            input_value: lsl_sp.StreamProcessor.INPUT_QUEUE_VALUE_TYPE
            input_value = self.input_queue.get()  # blocking read
            inlet, sample, timestamp = input_value
            corrected_timestamp = timestamp # + inlet.time_correction()
            stream_name: str = inlet.info().name()
            stream_info = xmltodict.parse(inlet.info().as_xml())
            channels = stream_info["info"]["desc"]["channels"]["channel"]
            number_of_npcs = (len(channels)+1) // 5

            NPC_chanID_dist = list(range(3,53,5))+list(range(63,143,5))
            min_distance = float("inf")
            nearest_npc_id = None
            for distance_channel_id in NPC_chanID_dist:
                distance_to_this_npc = sample[distance_channel_id]
                if distance_to_this_npc < min_distance:
                    min_distance = distance_to_this_npc
                    nearest_npc_id = distance_channel_id

            if nearest_npc_id is None:
                print("No NPC was found.")
            elif min_distance < MIN_DISTANCE_THRESHOLD: # Annahme Interaktionsszenario
                deviation_norm = sample[nearest_npc_id+1]
                average = deviation_norm
                normalized_average = average * 100.0 / 500.0
                if normalized_average > 100.0:
                    normalized_average = 100.0
                if normalized_average < 0.0:
                    normalized_average = 0.0
                normalized_average = 100.0 - normalized_average
                # print(corrected_timestamp, normalized_average)
                print("Interaktion: {}".format(normalized_average))
                self.outlet.push_sample([normalized_average], corrected_timestamp)
                
            else: # Annahme Vortragsszenario
                NPC_chanID_dev = list(range(4,54,5))+list(range(64,144,5))
                min_dev = float("inf")
                min_dev_npc_id = None
                for channel_id in NPC_chanID_dev:
                    dev_of_this_npc = sample[channel_id]
                    if dev_of_this_npc < min_dev:
                        min_dev = dev_of_this_npc
                        min_dev_npc_id = channel_id

                if min_dev_npc_id is None:
                    print("Vortragsszenario: No NPC was found.")

                average = min_dev
                normalized_average = average * 100.0 / 500.0
                if normalized_average > 100.0:
                    normalized_average = 100.0
                if normalized_average < 0.0:
                    normalized_average = 0.0
                normalized_average = 100.0 - normalized_average
                # print(corrected_timestamp, normalized_average)
                print("Vortrag: {}".format(normalized_average))
                self.outlet.push_sample([normalized_average], corrected_timestamp)


DEFAULT_MIN_NUMBER_OF_STREAMS = 1
parser = argparse.ArgumentParser(
    description='Computes the average of multiple streams.')
parser.add_argument("--min_number_of_streams", "-n",
                    dest="min_number_of_streams", type=int,
                    default=DEFAULT_MIN_NUMBER_OF_STREAMS,
                    help="Wait for at least this number of streams to be "
                         "available "
                         "before outputting their average, default: %d" %
                         DEFAULT_MIN_NUMBER_OF_STREAMS)
parser.add_argument("--predicate", "-p", dest='predicate', type=str,
                    default=SicherheitsverhaltenFusion.DEFAULT_LSL_PREDICATE,
                    help="The predicate that selects all relevant streams to "
                         "be averaged ."
                         "Default: {}".format(
                        SicherheitsverhaltenFusion.DEFAULT_LSL_PREDICATE))
args = parser.parse_args()

sicherheitsverhalten_fusion = SicherheitsverhaltenFusion(args.predicate, args.min_number_of_streams)
sicherheitsverhalten_fusion.output_loop()
